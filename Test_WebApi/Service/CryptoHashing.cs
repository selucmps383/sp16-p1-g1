﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Helpers;
using Test_WebApi;
using Test_WebApi.Models;

namespace Test_WebApi.Service
{
    public class CryptoHashing
    {
        SignalRContext db = new SignalRContext();

        public void SavePassword(string unhashedPassword)
        {
            string hashedPassword = Crypto.HashPassword(unhashedPassword);
        }

        public bool CheckPassword(string unhashedPassword, int userId)
        {
            //Gets Current User
            var user = db.Users.Find(userId);
            return CheckPassword(unhashedPassword, user);
        }

        public bool CheckPassword(string unhashedPassword, User user)
        {
            return user != null && Crypto.VerifyHashedPassword(user.Password, unhashedPassword);
        }
    }
}