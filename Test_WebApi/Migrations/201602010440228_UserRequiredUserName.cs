namespace Test_WebApi.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UserRequiredUserName : DbMigration
    {
        public override void Up()
        {
            DropIndex("dbo.Users", new[] { "UserName" });
            AlterColumn("dbo.Users", "UserName", c => c.String(nullable: false, maxLength: 512, unicode: false));
            AlterColumn("dbo.Users", "Password", c => c.String(nullable: false, maxLength: 512, unicode: false));
            CreateIndex("dbo.Users", "UserName", unique: true);
        }
        
        public override void Down()
        {
            DropIndex("dbo.Users", new[] { "UserName" });
            AlterColumn("dbo.Users", "Password", c => c.String(maxLength: 512, unicode: false));
            AlterColumn("dbo.Users", "UserName", c => c.String(maxLength: 512, unicode: false));
            CreateIndex("dbo.Users", "UserName", unique: true);
        }
    }
}
