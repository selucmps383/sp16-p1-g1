﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;
using System.Web.Security;
using Test_WebApi.Models;
using Test_WebApi.ViewModels;

namespace Test_WebApi.Controllers
{
    public class UserController : Controller
    {
        // GET: User
        [Authorize]
        public async Task<ActionResult> Index()
        {
            using (var db = new SignalRContext())
            {
                return View(await db.Users.ToListAsync());
            }
        }

        // GET: Login
        public ActionResult Login()
        {
            return View();
        }

        // POST: Login
        [HttpPost]
        public async Task<ActionResult> Login(string username, string password)
        {
            using (var db = new SignalRContext())
            {
                var loginUser = await db.Users.FirstOrDefaultAsync(u => u.UserName == username);
                if (loginUser != null)
                {
                    if (Crypto.VerifyHashedPassword(loginUser.Password, password))
                    {
                        FormsAuthentication.SetAuthCookie(loginUser.UserName, false);
                        return RedirectToAction("Index", "User");
                    }
                }
                ModelState.AddModelError("UserName", "User Name or Password was incorrect!");
                return View(new User() { UserName = username });
            }
        }

        // GET: Logout
        public ActionResult Logout()
        {
            FormsAuthentication.SignOut();
            return RedirectToAction("Login");
        }

        // GET: User/Create
        [Authorize]
        public ActionResult Create()
        {
            return View();
        }

        // POST: User/Create
        [HttpPost]
        [Authorize]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "FirstName,LastName,UserName,Password,ConfirmPassword")] UserVM newUser)
        {
            if (ModelState.IsValid)
            {
                using (var db = new SignalRContext())
                {
                    if (await db.Users.CountAsync(u => u.UserName == newUser.UserName) < 1)
                    {
                        newUser.Password = Crypto.HashPassword(newUser.Password);
                        db.Users.Add(newUser.ToUser());
                        await db.SaveChangesAsync();
                        return RedirectToAction("Index");
                    }
                    ModelState.AddModelError("Duplicate User Name", "That User Name already exists!");
                }
            }
            newUser.Password = "";
            newUser.ConfirmPassword = "";
            return View(newUser);
        }

        // GET: User/Details
        [Authorize]
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadGateway);
            }
            using (var db = new SignalRContext())
            {
                var detailUser = await db.Users.FindAsync(id);
                if (detailUser == null)
                {
                    return HttpNotFound("Could not find User to view!");
                }
                detailUser.Password = "";
                return View(detailUser);
            }
        }

        // GET: User/Edit/ID
        [Authorize]
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            using (var db = new SignalRContext())
            {
                var editUser = await db.Users.FindAsync(id);
                if (editUser == null)
                {
                    return HttpNotFound("Could not find User to edit!");
                }
                editUser.Password = "";
                return View(editUser.ToViewModel());
            }
        }

        // POST: User/Edit
        [HttpPost]
        [Authorize]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit(
            [Bind(Include = "ID,FirstName,LastName,UserName,Password,ConfirmPassword,Salt")] UserVM editUser)
        {
            if (ModelState.IsValid)
            {
                using (var db = new SignalRContext())
                {
                    var dbUser = await db.Users.FindAsync(editUser.ID);
                    if (dbUser == null)
                    {
                        return new HttpNotFoundResult("Could not find the User to edit!");
                    }
                    if (dbUser.UserName == editUser.UserName || await db.Users.CountAsync(u => u.UserName == editUser.UserName) == 0)
                    {
                        dbUser.Password = Crypto.HashPassword(editUser.Password);
                        dbUser.FirstName = editUser.FirstName;
                        dbUser.LastName = editUser.LastName;
                        dbUser.UserName = editUser.UserName;
                        db.Entry(dbUser).State = EntityState.Modified;
                        await db.SaveChangesAsync();
                        return RedirectToAction("Index");
                    }
                    else {
                        ModelState.AddModelError("Duplicate User Name", "That User Name already exists!");
                    }

                }
            }
            editUser.Password = "";
            editUser.ConfirmPassword = "";
            return View(editUser);
        }

        // GET: User/Delete/ID
        [Authorize]
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            using (var db = new SignalRContext())
            {
                var deleteUser = await db.Users.FindAsync(id);
                if (deleteUser == null)
                {
                    return HttpNotFound("Could not find User to delete!");
                }
                deleteUser.Password = "";
                return View(deleteUser);
            }
        }

        // POST: User/Delete/ID
        [Authorize]
        [ValidateAntiForgeryToken]
        [HttpPost, ActionName("Delete")]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            using (var db = new SignalRContext())
            {
                var deleteMe = await db.Users.FindAsync(id);
                if (await db.Users.CountAsync() > 1)
                {
                    db.Users.Remove(deleteMe);
                    await db.SaveChangesAsync();
                }
                return RedirectToAction("Index");
            }
        }
    }
}