﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.SignalR;
using Test_WebApi;
using Test_WebApi.Hubs;
using Test_WebApi.Models;
using WebGrease.ImageAssemble;

namespace Test_WebApi.Controllers
{
    public class InventoryController : Controller
    {
        private static readonly IHubContext Context = GlobalHost.ConnectionManager.GetHubContext<InventoryHub>();

        // GET: Inventory
        public ActionResult Index()
        {
            return View();
        }

        public async Task<JsonResult> GetInventory()
        {
            using (var db = new SignalRContext())
            {
                return Json(await db.IntentoryItems.Select(u => new { u.ID, u.Name, u.Quantity, u.Price }).ToListAsync(), JsonRequestBehavior.AllowGet);
            }
        }

        // GET: Inventory/Details/5
        [System.Web.Mvc.Authorize]
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            using (var db = new SignalRContext())
            {
                InventoryItem inventoryItem = await db.IntentoryItems.Include(i => i.User).SingleOrDefaultAsync(s => s.ID == id);
                if (inventoryItem == null)
                {
                    return HttpNotFound();
                }
                return View(inventoryItem);
            }
        }

        // GET: Inventory/Create
        [System.Web.Mvc.Authorize]
        public ActionResult Create()
        {
            return View();
        }

        // POST: Inventory/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [System.Web.Mvc.Authorize]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "ID,UserID,Name,Quantity,Price")] InventoryItem inventoryItem)
        {
            if (!ModelState.IsValid) return View(inventoryItem);
            using (var db = new SignalRContext())
            {
                inventoryItem.User =
                    await db.Users.FirstOrDefaultAsync(u => u.UserName == System.Web.HttpContext.Current.User.Identity.Name);
                db.IntentoryItems.Add(inventoryItem);
                await db.SaveChangesAsync();
                Context.Clients.All.addOrUpdate(new
                {
                    ID = inventoryItem.ID,
                    Name = inventoryItem.Name,
                    Quantity = inventoryItem.Quantity,
                    Price = inventoryItem.Price
                });
                return RedirectToAction(nameof(Index));
            }
        }

        // GET: Inventory/Edit/5
        [System.Web.Mvc.Authorize]
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            using (var db = new SignalRContext())
            {
                InventoryItem inventoryItem = await db.IntentoryItems.Include(i => i.User).SingleOrDefaultAsync(s => s.ID == id);
                if (inventoryItem == null)
                {
                    return HttpNotFound();
                }
                return View(inventoryItem);
            }
        }

        // POST: Inventory/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [System.Web.Mvc.Authorize]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "ID,UserID,Name,Quantity,Price")] InventoryItem inventoryItem)
        {
            if (!ModelState.IsValid) return View(inventoryItem);
            using (var db = new SignalRContext())
            {
                db.Entry(inventoryItem).State = EntityState.Modified;
                await db.SaveChangesAsync();
                Context.Clients.All.addOrUpdate(new
                {
                    ID = inventoryItem.ID,
                    Name = inventoryItem.Name,
                    Quantity = inventoryItem.Quantity,
                    Price = inventoryItem.Price
                });
                return RedirectToAction(nameof(Index));
            }
        }

        // GET: Inventory/Delete/5
        [System.Web.Mvc.Authorize]
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            using (var db = new SignalRContext())
            {
                InventoryItem inventoryItem = await db.IntentoryItems.FindAsync(id);
                if (inventoryItem == null)
                {
                    return HttpNotFound();
                }
                return View(inventoryItem);
            }
        }

        // POST: Inventory/Delete/5
        [System.Web.Mvc.Authorize]
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            using (var db = new SignalRContext())
            {
                InventoryItem inventoryItem = await db.IntentoryItems.FindAsync(id);
                db.IntentoryItems.Remove(inventoryItem);
                await db.SaveChangesAsync();
                Context.Clients.All.remove(new { ID = inventoryItem.ID });
            }
            return RedirectToAction(nameof(Index));
        }
    }
}
