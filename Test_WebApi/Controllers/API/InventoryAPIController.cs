﻿using Microsoft.AspNet.SignalR;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Mvc;
using Test_WebApi.Hubs;
using Test_WebApi.Models;

namespace Test_WebApi.Controllers.API_Controllers
{
    public class InventoryAPIController : ApiController
    {
        private static readonly IHubContext Context = GlobalHost.ConnectionManager.GetHubContext<InventoryHub>();

        // GET: api/InventoryAPI
        public async Task<IEnumerable<InventoryItem>> Get()
        {
            using (var db = new SignalRContext())
            {
                return await db.IntentoryItems.ToListAsync();
            }
        }

        // GET: api/InventoryAPI/5
        public async Task<InventoryItem> Get(int id)
        {
            using (var db = new SignalRContext())
            {
                return await db.IntentoryItems.FindAsync(id);
            }
        }

        // POST: api/InventoryAPI/5
        public HttpStatusCodeResult Post([FromBody] int purchaseID)
        {
            using (var db = new SignalRContext())
            {
                var inv = db.IntentoryItems.Find(purchaseID);
                inv.Quantity = Math.Max(inv.Quantity - 1, 0);
                db.Entry(inv).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();
                Context.Clients.All.addOrUpdate(new
                {
                    ID = inv.ID,
                    Name = inv.Name,
                    Quantity = inv.Quantity,
                    Price = inv.Price
                });
                return new HttpStatusCodeResult(HttpStatusCode.OK, "Successfully purchased item!");
            }
        }
    }
}

