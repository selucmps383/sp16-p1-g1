using Test_WebApi.Models;

namespace Test_WebApi
{
    using System;
    using System.Data.Entity;
    using System.Linq;

    public class SignalRContext : DbContext
    {
        // Your context has been configured to use a 'SignalRContext' connection string from your application's 
        // configuration file (App.config or Web.config). By default, this connection string targets the 
        // 'Test_WebApi.SignalRContext' database on your LocalDb instance. 
        // 
        // If you wish to target a different database and/or database provider, modify the 'SignalRContext' 
        // connection string in the application configuration file.
        public SignalRContext()
            : base("name=SignalRContext")
        {
        }

        // Add a DbSet for each entity type that you want to include in your model. For more information 
        // on configuring and using a Code First model, see http://go.microsoft.com/fwlink/?LinkId=390109.

        public virtual DbSet<User> Users { get; set; }
        public virtual DbSet<InventoryItem> IntentoryItems { get; set; }
        // public virtual DbSet<MyEntity> MyEntities { get; set; }
    }

    //public class MyEntity
    //{
    //    public int Id { get; set; }
    //    public string Name { get; set; }
    //}
}