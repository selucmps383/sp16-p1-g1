﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Drawing.Design;
using System.Linq;
using System.Web;
using Test_WebApi.Models;

namespace Test_WebApi.ViewModels
{
    [NotMapped]
    public class UserVM : User
    {
        [Required]
        [Compare("Password")]
        [DataType(DataType.Password)]
        [DisplayName("Confirm Password")]
        public string ConfirmPassword { get; set; }

        public User ToUser()
        {
            return new User()
            {
                FirstName = FirstName,
                ID = ID,
                LastName = LastName,
                Password = Password,
                UserName = UserName
            };
        }
    }
}