﻿$(function () {
    //Knockout View Model to display inventory
    var viewModel = {
        inventory: ko.observableArray()
    }
    $.ajax({
        url: "/Inventory/GetInventory",
        type: "GET",
        success: function (data) {
            try {
                $.each(data, function (index, value) {
                    viewModel.inventory.push({
                        ID: ko.observable(value.ID),
                        Name: ko.observable(value.Name),
                        Quantity: ko.observable(value.Quantity),
                        Price: ko.observable(convertToCurrency(value.Price))
                    });
                });
            } catch (ex) {
                console.error(ex);
            }
        },
        error: function (err) {
            console.error(err);
        }
    });

    ko.applyBindings(viewModel);

    var inventoryHub = $.connection.inventoryHub;
    inventoryHub.client.addOrUpdate = function (inventoryItem) {
        var exists = false;
        for (var i = 0; i < viewModel.inventory().length; i++) {
            if (viewModel.inventory()[i].ID() === inventoryItem.ID) {
                console.log("Changing record " + inventoryItem.ID);
                viewModel.inventory()[i].Name(inventoryItem.Name);
                viewModel.inventory()[i].Quantity(inventoryItem.Quantity);
                viewModel.inventory()[i].Price(convertToCurrency(inventoryItem.Price));
                exists = true;
            }
        };
        if (!exists) {
            console.log("Adding inventory item");
            viewModel.inventory.push(objToKnockout(inventoryItem));
        }
    };

    inventoryHub.client.remove = function (inventoryItem) {
        console.log(viewModel.inventory());
        viewModel.inventory.remove
        (
            function (item) {
                return item.ID() === inventoryItem.ID;
            }
        );
    }

    $.connection.hub.start();

    function objToKnockout(obj) {
        return {
            ID: ko.observable(obj.ID),
            Name: ko.observable(obj.Name),
            Quantity: ko.observable(obj.Quantity),
            Price: ko.observable(convertToCurrency(obj.Price))
        };
    }
});

function convertToCurrency(price) {
    console.log(Math.floor(price));
    var output = "";
    if (price < 0) {
        output += "-$" + price;
        if (Math.floor(price) === price) {
            output += ".00";
        }
    } else {
        output += "$" + price;
        if (Math.floor(price) === price) {
            output += ".00";
        }
    }
    return output;
}

function ViewDetails(btn) {
    location.href = "/Inventory/Details/" + $(btn).closest("tr").find("input:first").val();
}
function Edit(btn) {
    location.href = "/Inventory/Edit/" + $(btn).closest("tr").find("input:first").val();
}
function Delete(btn) {
    location.href = "/Inventory/Delete/" + $(btn).closest("tr").find("input:first").val();
}