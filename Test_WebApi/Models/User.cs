﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using System.Web.Helpers;
using Antlr.Runtime.Misc;
using Microsoft.Ajax.Utilities;
using Test_WebApi.ViewModels;

namespace Test_WebApi.Models
{
    public class User
    {
        [Key]
        public int ID { get; set; }

        [Column(TypeName = "varchar")]
        [DisplayName("First Name")]
        [StringLength(512, ErrorMessage = "First Name can be no more than 512 characters")]
        public string FirstName { get; set; }

        [Column(TypeName = "varchar")]
        [DisplayName("Last Name")]
        [StringLength(512, ErrorMessage = "Last Name can be no more than 512 characters")]
        public string LastName { get; set; }

        [Required]
        [Column(TypeName = "varchar")]
        [Index(IsUnique = true)]
        [DisplayName("User Name")]
        [StringLength(512, ErrorMessage = "User Name can be no more than 512 characters")]
        public string UserName { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Column(TypeName = "varchar")]
        [MinLength(8, ErrorMessage = "Password must be more than 7 characters")]
        [StringLength(512, ErrorMessage = "Password can be no more than 512 characters")]
        public string Password { get; set; }

        public UserVM ToViewModel()
        {
            return new UserVM()
            {
                FirstName = FirstName,
                ID = ID,
                LastName = LastName,
                UserName = UserName
            };
        }
    }
}