﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Test_WebApi.Models
{
    public class InventoryItem
    {
        [Key]
        public int ID { get; set; }

        [ForeignKey(nameof(User))]
        public int UserID { get; set; }
        public User User { get; set; }

        [Column(TypeName = "varchar")]
        [StringLength(512, ErrorMessage = "Inventory Item Name can be no more than 512 characters")]
        public string Name { get; set; }

        [Range(0,Int32.MaxValue)]
        public int Quantity { get; set; }

        [Range(0,Int32.MaxValue)]
        [DataType(DataType.Currency)]
        public decimal Price { get; set; }
    }
}